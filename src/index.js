const express = require('express')
const sum = require('./util').sum

const app = express()

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
})

app.get('/', (_req, res) => {
    res.status(200).send('Hello cloud world!')
})

app.get('/sum', (req, res) => {
    const first = Number(req.query.first)
    const second = Number(req.query.second)
    try {
        const result = sum(first, second)
        const message = `The sum of ${first} and ${second} is ${result}`
        res.status(200).send(message)
    } catch (error) {
        res.status(400).send(error.message)
    }
})

const port = process.env.PORT || 80
const environment = process.env.NODE_ENV 

if (environment !== 'test') {
    app.listen(port, () => {
        console.log(`Application is listening to port ${port}`)
    })
}

module.exports = app