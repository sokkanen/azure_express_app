const sum = (a,b) => {
    if ((typeof a !== 'number' || isNaN(a)) || (typeof b !== 'number' || isNaN(b))) {
        throw new Error(`Either one or both parameters are invalid: ${a}, ${b}`)
    }
    return a + b
}

module.exports = { sum }