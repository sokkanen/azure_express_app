const request = require("supertest")
const app = require('../src/index')

describe('Testing root', () => {
    it('returns 200 with a message', (done) => {
        request(app)
            .get('/')
            .expect(200)
            .expect('Hello cloud world!', done)
    })
})

describe('Testing params endpoint', () => {
    // Happy case
    it('returns 200 with proper params', (done) => {
        request(app)
            .get('/sum?first=4&second=2')
            .expect(200)
            .expect('The sum of 4 and 2 is 6', done)
    })
    // Unhappy cases
    it('returns 400 with other param missing', (done) => {
        request(app)
            .get('/sum?first=4')
            .expect(400)
            .expect('Either one or both parameters are invalid: 4, NaN', done)
    })
    it('returns 400 with both parameters missing', (done) => {
        request(app)
            .get('/sum')
            .expect(400)
            .expect('Either one or both parameters are invalid: NaN, NaN', done)
    })
})