const sum = require('../src/util').sum

// Happy case
test('Sum returns 4 with parameters 1 and 3', () => {
    const result = sum(1,3)
    expect(result).toBe(4)
})

// Unhappy cases
test('Sum throws an error with string parameter ', () => {
    expect(() => {
        sum(4, 'raimo')
      }).toThrow()
})

test('Sum throws an error with boolean parameter ', () => {
    expect(() => {
        sum(4, true)
      }).toThrow()
})

test('Sum throws an error without parameters', () => {
    expect(() => {
        sum()
      }).toThrow()
})

test('Sum throws an error with one parameter', () => {
    expect(() => {
        sum(4)
      }).toThrow()
})